#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include <fftw3.h>
#include "utils.h"
#include "wavreader.h"
#include "inireader.h"
#include "strategien.h"

//#define DEBUG

int main()
{
    /* fftw_complex *out; */
    /* fftw_plan p; */
    /* double *in; */
    /* int N = 8; */
    /* int i; */

    /* in = (double*) fftw_malloc(sizeof(double) * N); */
    /* out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N); */

    /* for (i = 0; i < N; ++i) */
    /* { */
    /*     printf("%6.2f+j%6.2f\n", out[i][0],out[i][1]); */
    /*     out[i][1] = out[i][0] = 1; */
    /* } */
    /* printf("\n\n"); */

    /* for (i = 0; i < N; i++) */
    /* { */
    /*     in[i] = 1.0; */
    /*     /\* in[i][1] = 0.0; *\/ */
    /* } */

    /* p = fftw_plan_dft_r2c_1d(N, in, out, FFTW_ESTIMATE); */
    /* fftw_execute(p); */

    /* for (i = 0; i < N; i++) */
    /* { */
    /*     //printf("%6.2f", sqrt(SQR(out[i][0]) + SQR(out[i][1]))); */
    /*     printf("%6.2f+j%6.2f\n", out[i][0],out[i][1]); */
    /* } */
    /* printf("\n"); */
    /* fftw_destroy_plan(p); */
    /* fftw_free(in); */
    /* fftw_free(out); */

    struct IniFile * strategyIniFile;
    struct IniFile * globalIniFile;
    struct AlgorithmusConfig config;
    struct GlobalConfig gConfig;
    struct WavHeader *inputHeader;
    struct WavHeader *outputHeader;
    double * dWindow;
    double * dWinFunc;
    double * dFrame;
    double * dSynthesed;
    double * testFFT;
    double * dElektroden;
    FILE *fh;
    FILE *fho;
    int iDataRead,i, debugCounter;
    /* //fh = fopen("./algorithmen/ACE.ini", "r"); */

    fh = fopen("./configure.cfg", "r");

    if (fh == NULL)
    {
        printf("Error: Cannot open file configure.cfg\n");
        return 1;
    }

    globalIniFile = ReadInitFile(fh);
    //PrintIniFile(globalIniFile);
    gConfig = InterprateGlobalConfig(*globalIniFile);
    // printf("%s\n", gConfig.sAlgorithmus);

    fclose(fh);

    fh = fopen(gConfig.sAlgorithmus, "r");
    //fh = fopen("./algorithmen/APACEv.ini","r");

    if (fh == NULL)
    {
        printf("Error: Cannot open file %s\n",gConfig.sAlgorithmus);
        return 1;
    }

    strategyIniFile = ReadInitFile(fh);

    fclose(fh);

    //PrintIniFile(strategyIniFile);

    config = InterprateInitFile(*strategyIniFile);
    //PrintAlgorithmusConfig(config);
    //dWindow = malloc(sizeof(double) * config.iWindowSize);

    dWindow = calloc(config.iWindowSize, sizeof(double));
    // dWindow = (double *) fftw_malloc(sizeof(double) * config.iWindowSize);
    dWinFunc = Hanning(config.iWindowSize);

    dFrame = malloc(sizeof(double) * config.iFrameSize);
    dSynthesed = malloc(sizeof(double) * config.iFrameSize);
    if (gConfig.input == WAVFILE)
    {
        fh = fopen(gConfig.sInputFile, "rb+");
        inputHeader = readWaveHeader(fh);
        debugCounter = 0;
        if (gConfig.output == WAVFILE)
        {
            fho = fopen(gConfig.sOutputFile, "wb+");
            outputHeader = createWaveHeader(inputHeader->iChannels, config.iSamplerate, inputHeader->iBitsPerSample);
            WaveWriteHeader(fho, outputHeader);
            // printWaveHeader(outputHeader);
            while ((iDataRead = WaveReadFrame(fh, inputHeader, config.iFrameSize, dFrame)) > 0)
            {

                for (i = iDataRead; i < config.iFrameSize; ++i)
                {
                    dFrame[i] == 0.0;
                }

                BufferFrame(dWindow, config.iWindowSize, dFrame, config.iFrameSize);

                /* for (i = 0; i < config.iWindowSize; ++i) */
                /* { */
                /*     dWindow[i] = dWindow[i] * dWinFunc[i]; */
                /* } */

                switch (config.eStrategy)
                {
                    case ACE:
                        {
                            dElektroden = ACEStrategie(config, dWindow, dSynthesed, 0);
                            break;
                        }
                    case PACE:
                        {
                            dElektroden = PACEStrategie(config, dWindow, dSynthesed, 0);
                            break;
                        }
                    case APACEv:
                        {
                            dElektroden = APACEvStrategie(config, dWindow, dSynthesed, 0);
                            break;
                        }
                    default:
                        return -1;
                        break;
                }
                //dElektroden = ACEStrategie(config, dWindow, dSynthesed, 0);
                //dElektroden = PACEStrategie(config, dWindow, dSynthesed, 0);

                WaveWriteFrame(fho, outputHeader, config.iFrameSize, dSynthesed);

                free(dElektroden);
                /*
                if (++debugCounter >= 1)
                  {
                  break;
                  }
                */

            }
            WaveWriteHeader(fho, outputHeader);
            // printWaveHeader(outputHeader);
            fclose(fho);
            fclose(fh);
        }

    }
    free(dWindow);

    return 0;
}
