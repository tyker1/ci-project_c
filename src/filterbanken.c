#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "utils.h"
#include "filterbanken.h"

fftw_plan fft_InitFilter(int n, double * source, fftw_complex * fftoutput)
{
    return fftw_plan_dft_r2c_1d(n, source, fftoutput, FFTW_ESTIMATE);
}

double * fft_FilterBank(struct AlgorithmusConfig config, double * source, fftw_complex * fftoutput, fftw_plan plan)
{
    int i,j;
    int start, end;
    double gain;
    double * result;

    result = malloc(sizeof(double) * config.iNumFilters);

    fftw_execute(plan);

    /* for (i = 0; i < config.iWindowSize / 2 + 1; ++i) */
    /* { */
    /*     printf("%.5e ", sqrt(SQR(fftoutput[i][0]) + SQR(fftoutput[i][1])) / config.iWindowSize * 2); */
    /* } */
    /* printf("\n**************************************************\n"); */

    for (i = 0; i < config.iNumFilters; ++i)
    {
        result[i] = 0;

        start = round(config.b[i][0]);
        end = round(config.b[i][1]);
        gain = config.a[i][0];
        //printf("start = %d\tend=%d\tgain=%.3f\n",start,end,gain);
        for (j = start; j <= end; ++j)
        {
            result[i] = result[i] + gain * (SQR(fftoutput[j][0]) + SQR(fftoutput[j][1]));
        }

        result[i] = sqrt(result[i]) / config.iWindowSize * 2;

    }

    return result;
}
