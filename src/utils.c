#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include "utils.h"

struct Range {
    int start, end;
};

struct Range new_Range(int s, int e)
{
    struct Range r;
    r.start = s;
    r.end = e;
    return r;
}

double * Hanning(int n)
{
    double * dResult;
    int i;

    dResult = malloc(sizeof(double) * n);

    for (i = 0; i < n; ++i)
    {
        dResult[i] = 0.5 * (1 - cos(2 * M_PI *i / (n-1)));
    }

    return dResult;

}

void QuickSort(double * arr, const int len, int * index)
{
    double mid,dTemp;
    int right,left, iTemp;
    struct Range r[len];
    struct Range range;
    int midIdx;
    int p = 0;
    int i;

    if (len <= 0)
    {
        return;
    }

    r[p] = new_Range(0, len - 1);
    p = p + 1;
    while (p)
    {
        range = r[p-1];
        p = p - 1;
        if (range.start >= range.end)
        {
            continue;
        }

        mid = arr[(range.start + range.end) / 2];
        left = range.start;
        right = range.end;
        do
        {
            while (arr[left] > mid) ++left;
            while (arr[right] < mid) --right;

            if (left <= right)
            {
                dTemp = arr[left];
                iTemp = index[left];
                arr[left] = arr[right];
                index[left] = index[right];
                arr[right] = dTemp;
                index[right] = iTemp;
                left++;right--;
            }

        } while (left <= right);
        if (range.start < right) r[p++] = new_Range(range.start, right);
        if (range.end > left) r[p++] = new_Range(left, range.end);
        /* midIdx = (range.start + range.end) / 2; */
        /* mid = arr[midIdx]; */
        /* left = range.start; */
        /* right = range.end; */
        /* printf("midIdx = %d\tmid=%.5e\tleft=%d\tright=%d\n",midIdx,mid,left,right); */
        /* while (left < right) */
        /* { */
        /*     while ((arr[left] >= mid) && (left < midIdx)) */
        /*     { */
        /*         left++; */
        /*     } */
        /*     while ((arr[right] < mid) && (midIdx < right)) */
        /*     { */
        /*         right--; */
        /*     } */
        /*     dTemp = arr[left]; */
        /*     iTemp = index[left]; */
        /*     arr[left] = arr[right]; */
        /*     index[left] = index[right]; */
        /*     arr[right] = dTemp; */
        /*     index[right] = iTemp; */
        /*     left++; */
        /*     right--; */
        /* } */
        /* r[p++] = new_Range(range.start, left - 1); */
        /* r[p++] = new_Range(right + 1, range.end); */

    }

}

double str2double(char * str)
{
    double result = 0;
    double factor = 1;
    // integer part
    while ((*str != '\0') && (*str != '.'))
    {
        result = result * 10 + 1.0*(*str - '0');
        str++;
    }
    str++;
    while (*str != '\0')
    {
        result = result * 10 + 1.0 * (*str - '0');
        str++;
        factor = factor * 10;
    }

    return result / factor;
}

double * arrIntToDouble(int *arr, int arrlen, int norm)
{
    double * out;
    int i;

    out = malloc(sizeof(double) * arrlen);

    for (i = 0; i < arrlen; ++i)
    {
        out[i] = 1.0 * arr[i] / norm;
    }

    return out;
}

void BufferFrame(double * buffer, int bufferLen , double * frame, int frameLen)
{
    if (bufferLen < frameLen)
    {
        return;
    }

    memcpy(buffer, buffer+frameLen, (bufferLen-frameLen)*sizeof(double));
    memcpy(buffer+bufferLen-frameLen, frame, frameLen*sizeof(double));
}

void RemoveWhiteSpaces(char * str)
{
    int i,j;

    i = strlen(str) - 1;
    while ((str[i] == ' ') || (str[i] == '\n' ))
    {
        i--;
    }
    str[i+1] = '\0';

    j = 0;

    while (str[j] == ' ')
    {
        j++;
    }

    for (i = 0; str[j] != '\0'; ++i,++j)
    {
        str[i] = str[j];
    }
    str[i] = '\0';
}

int Str2Int(char * str)
{
    int result = 0;

    while (*str != '\0')
    {
        result = result * 10 + (*str - '0');
        str++;
    }

    return result;
}

struct GlobalConfig InterprateGlobalConfig(struct IniFile ini)
{
    char algorithmusPath[256] = "./algorithmen/";
    struct GlobalConfig gConfig;
    struct IniSection iniSec;
    char * key;
    char * value;
    int i;

    if (ini.iNumSections > 1)
    {
        printf("Not a valid Configuration file\n");
        return gConfig;
    }

    iniSec = ini.iniSections[0];

    for (i = 0; i < iniSec.iNumberofKey; ++i)
    {
        key = iniSec.asKeys[i];
        if (strcmp(key, "algorithmus") == 0)
        {
            strcat(algorithmusPath, iniSec.asValues[i]);
            gConfig.sAlgorithmus = malloc((strlen(algorithmusPath)+1) * sizeof(char));
            strcpy(gConfig.sAlgorithmus, algorithmusPath);
        } else if (strcmp(key, "input") == 0)
        {
            value = iniSec.asValues[i];
            if (strcmp(value, "file") == 0)
            {
                gConfig.input = WAVFILE;
            }else
            {
                gConfig.input = MIC;
            }
        } else if (strcmp(key, "output") == 0)
        {
            value = iniSec.asValues[i];
            if (strcmp(value, "file") == 0)
            {
                gConfig.output = WAVFILE;
            } else
            {
                gConfig.output = SPEAKER;
            }
        } else if (strcmp(key, "inputFile") == 0)
        {
            gConfig.sInputFile = iniSec.asValues[i];
        } else if (strcmp(key, "outputFile") == 0)
        {
            gConfig.sOutputFile = iniSec.asValues[i];
        }
    }

    return gConfig;
}

void ReadFilterConfiguration(struct AlgorithmusConfig * config, char * fileName)
{
    char filePath[256] = "./filters/";
    char dataCheck[5] = {0};
    int32_t iSettings[4];
    FILE *fh;
    int i,j;
    strcat(filePath, fileName);
    fh = fopen(filePath, "rb");
    if (fh == NULL)
    {
        printf("Error! Cannot open file %s\n", filePath);
        return;
    }

    fread(dataCheck, sizeof(char), 4, fh);
    if (strcmp(dataCheck, "DATA") != 0)
    {
        printf("Error, data not valid, first 4 Bytes read = %s\n",dataCheck);
        return;
    }

    fread(iSettings, sizeof(int32_t), 4, fh);

    // malloc memory for filter coeffizients, also using linear memory
    config->a = malloc(sizeof(double *) * iSettings[1]);
    config->a[0] = malloc(sizeof(double) * iSettings[1] * iSettings[2]);
    config->b = malloc(sizeof(double *) * iSettings[1]);
    config->b[0] = malloc(sizeof(double) * iSettings[1] * iSettings[3]);
    config->dMidFreq = malloc(sizeof(double) * iSettings[1]);
    config->iNumFilters = iSettings[1];

    config->iFilterLen[0] = iSettings[2];
    config->iFilterLen[1] = iSettings[3];

    for (i = 1; i < iSettings[1]; ++i)
    {
        config->a[i] = config->a[0] + iSettings[2] * i;
        config->b[i] = config->b[0] + iSettings[3] * i;
    }
    for (i = 0; i < iSettings[1]; ++i)
    {
        fread(config->dMidFreq + i, sizeof(double), 1, fh);
        fread(config->a[i], sizeof(double), iSettings[2], fh);
        fread(config->b[i], sizeof(double), iSettings[3], fh);
        //printf("MidF = %.3f\tGain=%.3f\tStart=%.3f\tEnd=%.3f\n",config->dMidFreq[i],config->a[i][0],config->b[i][0],config->b[i][1]);
    }


}

struct AlgorithmusConfig InterprateInitFile(struct IniFile iniFile)
{
    struct AlgorithmusConfig config;
    struct IniSection iniSec;
    char * key;
    char * value;
    char * fileName;
    int i;

    if (iniFile.iNumSections > 1) // each init file should only contain 1 section
    {
        config.iSamplerate = -1;
        return config;
    }

    iniSec = iniFile.iniSections[0];

    if (strcmp(iniSec.sName, "ACE") == 0)
    {
        config.eStrategy = ACE;
    } else if (strcmp(iniSec.sName,"PACE") == 0)
    {
        config.eStrategy = PACE;
    } else if (strcmp(iniSec.sName, "APACEv") == 0)
    {
        config.eStrategy = APACEv;
    } else if (strcmp(iniSec.sName, "HiRes") == 0)
    {
        config.eStrategy = HiRes;
    } else if (strcmp(iniSec.sName, "FSP") == 0)
    {
        config.eStrategy =FSP;
    } else
    {
        config.eStrategy = CUSTOM;
    }

    for (i = 0; i < iniSec.iNumberofKey; ++i)
    {
        key = iniSec.asKeys[i];
        value = iniSec.asValues[i];

        if (strcmp(key, "numChannel") == 0)
        {
            config.iNumChannel = Str2Int(value);
        }else if (strcmp(key, "numVirtchannel") == 0)
        {
            config.iNumVirtChannel = Str2Int(value);
        }else if (strcmp(key, "Samplerate") == 0)
        {
            config.iSamplerate = Str2Int(value);
        }else if (strcmp(key, "selection") == 0)
        {
            config.bHaveSelection = Str2Int(value);
        }else if (strcmp(key, "selectionModel") == 0)
        {
            config.iSelectionMode = Str2Int(value);
        }else if (strcmp(key, "numSelected") == 0)
        {
            config.iNumSelected = Str2Int(value);
        }else if (strcmp(key, "filterbank") == 0)
        {
            if (strcmp(value, "fft") == 0)
            {
                config.iTypeFilterbank = 0;
            }else if (strcmp(value, "iir") == 0)
            {
                config.iTypeFilterbank = 2;
            }else if (strcmp(value, "fir") == 0)
            {
                config.iTypeFilterbank = 1;
            }else
            {
                config.iTypeFilterbank = -1;
                return config;
            }
        }else if (strcmp(key, "synthesModel") == 0)
        {
            if (strcmp(value, "sin") == 0)
            {
                config.iSyntheseModel = SINUS;
            }else if (strcmp(value, "noise") == 0)
            {
                config.iSyntheseModel = NOISE;
            }else
            {
                config.iSyntheseModel = UNKNOWN;
                return config;
            }
        }else if (strcmp(key, "FilterTable") == 0)
        {
            fileName = value;
        }else if (strcmp(key, "WindowSize") == 0)
        {
            config.iWindowSize = Str2Int(value);
        }else if (strcmp(key, "FrameSize") == 0)
        {
            config.iFrameSize = Str2Int(value);
        }else if (strcmp(key, "av") == 0)
        {
            config.dAttenuation = str2double(value);
        }else if (strcmp(key, "sl") == 0)
        {
            config.dLeftSlope = str2double(value);
        }else if (strcmp(key, "sr") == 0)
        {
            config.dRightSlope = str2double(value);
        }else if (strcmp(key, "alpha") == 0)
        {
            config.dAlpha = str2double(value);
        }
    }

    if (config.iNumVirtChannel != 0)
    {
        config.iNumFilters = config.iNumChannel * config.iNumVirtChannel;
    }else
    {
        config.iNumFilters = config.iNumChannel;
    }

    ReadFilterConfiguration(&config, fileName);

    return config;
}

void PrintAlgorithmusConfig(struct AlgorithmusConfig config)
{
    printf("====================================================\n");
    printf("No. of Channel = %d\n",config.iNumChannel);
    printf("VirtChannel between each Channel = %d\n", config.iNumVirtChannel);
    printf("Total Filters = %d\n", config.iNumFilters);
    printf("Samplerate = %d\n", config.iSamplerate);
    printf("Selection = %s\n", (config.bHaveSelection == 1 ? "yes" : "no"));
    switch (config.iSelectionMode)
    {
        case 0:
            {
                printf("Selection Mode = Custom\n");
                break;
            }
        case 1:
            {
                printf("Selection Mode = Maximun\n");
                break;
            }
        case 2:
            {
                printf("Selection Mode = Psychoakustic\n");
                break;
            }
        default:
            printf("Selection Mode = Error/Unknown\n");
            break;
    }
    printf("No. of Selected Channels = %d\n",config.iNumSelected);
    printf("Window Size = %d\n", config.iWindowSize);
    printf("Frame Size = %d\n", config.iFrameSize);
    switch (config.iTypeFilterbank)
    {
        case 0:
            {
                printf("Filter Type = fft\n");
                break;
            }
        case 1:
            {
                printf("Filter Type = fir\n");
                break;
            }
        case 2:
            {
                printf("Filter Type = iir\n");
                break;
            }
        default:
            printf("Filter Type Error/Unkonwn\n");
            break;
    }
    switch (config.iSyntheseModel)
    {
        case 0:
            {
                printf("Synthese Modell = Sinusoidal Synthese\n");
                break;
            }
        case 1:
            {
                printf("Synthese Modell = Noise Vocoder\n");
                break;
            }
        default:
            printf("Synthese Modell Error/Unknow\n");
            break;
    }
    printf("Attenuation = %.3f\n", config.dAttenuation);
    printf("Left Slope = %.3f\n", config.dLeftSlope);
    printf("Right Slope = %.3f\n", config.dRightSlope);
    printf("Alpha = %.3f\n", config.dAlpha);
    printf("First MidFreq = %.3f\t Last = %.3f\n",config.dMidFreq[0], config.dMidFreq[config.iNumFilters-1]);
}
