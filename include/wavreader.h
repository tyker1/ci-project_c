#ifndef __WAVREADER_H__
#define __WAVREADER_H__

#include <stdint.h>
#include <stdio.h>

struct WavHeader {
    char cRiff[4];
    uint32_t uiFileSize;
    char cWaveMark[4];
    char cFmtMarker[4];
    uint32_t iFmtLen;
    uint16_t iFmtType;
    uint16_t iChannels;
    uint32_t iSampleRate;
    uint32_t iByteRate;
    uint16_t iAlignment;
    uint16_t iBitsPerSample;
    char cData[4];
    uint32_t iDataSize;
};

struct WavHeader * readWaveHeader (FILE * fh);
struct WavHeader * createWaveHeader (uint16_t channels, uint32_t samplerate, uint16_t bits);
void printWaveHeader (struct WavHeader * header);
int WaveReadFrame(FILE *fh, struct WavHeader * header,int iFrameSize, double *output);
int WaveWriteFrame(FILE *fh, struct WavHeader * header, int iFrameSize, double *frame);
void WaveWriteHeader(FILE *fh, struct WavHeader * header);
int CheckWavEnd(struct WavHeader * header, int reset);

#endif /* __WAVREADER_H__ */
