#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

int main()
{
    char test[8] = {0x0A, 0x05, 0x10, 0x1f, 0x0C,0x0D, 0x50, 0x9b};
    int32_t i32Test[2];
    double * doubleTest;
    int16_t i16Test[4];
    int i,len;
    int32_t a;
    int16_t b;
    b = -1;
    a = b;

    printf("%d ", (int16_t)a);
    printf("%d \n", a);
    a = 32768;
    printf("%d ", (int16_t)a);
    printf("%d \n", a);
    memcpy(i32Test, test, sizeof(test));
    memcpy(i16Test, test, sizeof(test));

    printf("Raw Data in Byte Form\n");
    len = sizeof(test);
    for (i = 0; i < len; ++i)
    {
        printf("%d ", test[i]);
    }

    doubleTest = malloc(sizeof(double) * 5);

    for (i = 0; i < 5; ++i)
    {
        doubleTest[i] = i*10.0 + 1.0;
    }
    printf("\n");
    for (i = 0; i < 5; ++i)
    {
        printf("Value = %.3f\tAddress=%p\tAddress&=%p\n", *(doubleTest+i), doubleTest+i, &doubleTest[i]);
        printf("Value Calculated = %p\n", doubleTest + i * sizeof(double));
    }


    printf("\nInt32 Data\n");
    len = sizeof(i32Test)/sizeof(i32Test[0]);
    for (i = 0; i < len; ++i)
    {
        printf("%d ", i32Test[i]);
    }
    printf("\nInt16 Data\n");
    len = sizeof(i16Test)/sizeof(i16Test[0]);
    for (i = 0; i < len; ++i)
    {
        printf("%d ", i16Test[i]);
    }
    printf("\n");

    printf("%d \n", (1 << 15));
    printf("%d \n", (1 << (sizeof(int32_t)*8-1)) - 1);
    return 0;
}
