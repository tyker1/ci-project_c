#ifndef __FILTERBANKEN_H__
#define __FILTERBANKEN_H__

#include <fftw3.h>

fftw_plan fft_InitFilter(int n, double * source, fftw_complex * fftoutput);
double * fft_FilterBank(struct AlgorithmusConfig config, double * source, fftw_complex* fftoutput, fftw_plan plan);


#endif /* FILTERBANKEN_H */
