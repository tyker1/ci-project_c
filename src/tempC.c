#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct AlgorithmusConfig{
    int iNumChannel;
    int iNumVirtChannel;
    int bHaveSelection; // equals 1 if theres selection needed, otherweis 0
    int iSelectionMode; // if bHaveSelection set to 1, 0 for custom selection,1 for maximun, 2 for psychoacustic
    int iNumSelected; // no use if theres no selection
    int iSamplerate;
    int iTypeFilterbank; // 0 = fft, 1 = FIR, 2 = IIR
    int iNumFilters;
    int iWindowSize;
    int iFrameSize;
    double * * a;        // for fir & iir, if it is FFT, a,b contains the scaling(a)
    double * * b;        // and FFT-Bin range (b, always array of 2 elements for start and end)
    double * dMidFreq;    // for Midfrequences at each Channel
    int iFilterLen[2];      // for fir & iir, array tells how many elements of a & b each Channel
    int iSyntheseModel; // 0 for Sinuston, 1 for Noise
};

void ReadFilterConfiguration(struct AlgorithmusConfig * config, char * fileName)
{
    char filePath[256] = "./filters/";
    char dataCheck[5] = {0};
    int32_t iSettings[4];
    FILE *fh;
    int i,j;
    strcat(filePath, fileName);
    fh = fopen(filePath, "r");
    if (fh == NULL)
    {
        printf("Error! Cannot open file %s\n", filePath);
        return;
    }

    fread(dataCheck, sizeof(char), 4, fh);
    if (strcmp(dataCheck, "DATA") != 0)
    {
        printf("Error, data not valid, first 4 Bytes read = %s\n",dataCheck);
        return;
    }

    fread(iSettings, sizeof(int32_t), 4, fh);

    printf("Read: %d\t%d\t%d\t%d\n",iSettings[0],iSettings[1],iSettings[2],iSettings[3]);

    // malloc memory for filter coeffizients, also using linear memory
    config->a = malloc(sizeof(double *) * iSettings[1]);
    config->a[0] = malloc(sizeof(double) * iSettings[1] * iSettings[2]);
    config->b = malloc(sizeof(double *) * iSettings[1]);
    config->b[0] = malloc(sizeof(double) * iSettings[1] * iSettings[3]);
    config->dMidFreq = malloc(sizeof(double) * iSettings[1]);

    for (i = 1; i < iSettings[1]; ++i)
    {
        config->a[i] = config->a[0] + iSettings[2] * i;
        config->b[i] = config->b[0] + iSettings[3] * i;
    }
    printf("File Position = %X\n",ftell(fh));
    for (i = 0; i < iSettings[1]; ++i)
    {
        fread(config->dMidFreq + i, sizeof(double), 1, fh);
        printf("MidFreq[%d] = %.3f\n",i,config->dMidFreq[i]);
        printf("File Position = %X\n", ftell(fh));
        fread(config->a[i], sizeof(double), iSettings[2], fh);
        printf("File Position = %X\n", ftell(fh));
        fread(config->b[i], sizeof(double), iSettings[3], fh);
        printf("File Position = %X\n", ftell(fh));
    }


}

int int main(int argc, char *argv[])
{

    return 0;
}
