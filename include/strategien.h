#ifndef __STRATEGIEN_H__
#define __STRATEGIEN_H__

double * ACEStrategie(struct AlgorithmusConfig config, double * dFrame, double * dSynthesed, int iReset);
double * PACEStrategie(struct AlgorithmusConfig config, double * dFrame, double * dSynthesed, int iReset);
double * APACEvStrategie(struct AlgorithmusConfig config, double * dFrame, double * dSynthesed, int iReset);






#endif /* __STRATEGIEN_H__ */
