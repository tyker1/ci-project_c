#ifndef __UTILS_H__
#define __UTILS_H__

#include "inireader.h"
#include <math.h>

#define FFTSIZE 4096
#define SAMPLERATE 44100
#define SELECTEDCHANNELS 6
#define FRAMESIZE 512
#define MAX_CHANNEL 256
#define M_PI 3.14159265358979323846

#define SQR(x) ((x)*(x))
#define MAG2DB(x) (10 * log10(x / 1e-12))

enum IOType{
    WAVFILE,MIC,SPEAKER
};

struct GlobalConfig{
    char * sAlgorithmus;
    char * sInputFile;
    char * sOutputFile;
    enum IOType input;
    enum IOType output;
};

enum SyntheseType{
    SINUS,NOISE, UNKNOWN
};

enum Strategy{
    ACE, PACE, APACEv ,HiRes, FSP, CUSTOM
};

struct AlgorithmusConfig{
    enum Strategy eStrategy;
    int iNumChannel;
    int iNumVirtChannel;
    int bHaveSelection; // equals 1 if theres selection needed, otherweis 0
    int iSelectionMode; // if bHaveSelection set to 1, 0 for custom selection,1 for maximun, 2 for psychoacustic
    int iNumSelected; // no use if theres no selection
    int iSamplerate;
    int iTypeFilterbank; // 0 = fft, 1 = FIR, 2 = IIR
    int iNumFilters;
    int iWindowSize;
    int iFrameSize;
    double * * a;        // for fir & iir, if it is FFT, a,b contains the scaling(a)
    double * * b;        // and FFT-Bin range (b, always array of 2 elements for start and end)
    double * dMidFreq;    // for Midfrequences at each Channel
    int iFilterLen[2];      // for fir & iir, array tells how many elements of a & b each Channel
    enum SyntheseType iSyntheseModel; // 0 for Sinuston, 1 for Noise
    // below are parameter for Psychoacustical Modell
    double dAttenuation;
    double dLeftSlope;
    double dRightSlope;
    double dAlpha;
};

void BufferFrame(double * buffer, int bufferLen, double * frame, int frameLen);
double * arrIntToDouble(int * arr, int arrlen, int norm);
void RemoveWhiteSpaces(char * str);
int Str2Int(char * str);
struct AlgorithmusConfig InterprateInitFile(struct IniFile iniFile);
void PrintAlgorithmusConfig(struct AlgorithmusConfig config);
void QuickSort(double * arr, int len, int * index);
struct GlobalConfig InterprateGlobalConfig(struct IniFile ini);
double * Hanning(int n);











#endif
