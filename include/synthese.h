#ifndef __SYNTHESE_H__
#define __SYNTHESE_H__

#include "utils.h"

void SinusSynthese(struct AlgorithmusConfig config, double * dEnvelope, double * dSynthesed);
void AudioSynthese(struct AlgorithmusConfig config, double * dEnvelope, double * dSynthesed);





#endif /* __SYNTHESE_H__ */
