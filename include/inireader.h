#ifndef __INIREADER_H__
#define __INIREADER_H__

#include <stdio.h>

struct IniSection {
    char * sName;
    int iNumberofKey;
    char * * asKeys;
    char * * asValues;
};

struct IniFile{
    int iNumSections;
    struct IniSection * iniSections;
};

struct IniSection ReadInitSection(FILE* fh);
struct IniFile* ReadInitFile(FILE* fh);
void PrintIniSection(struct IniSection iniSec);
void PrintIniFile(struct IniFile* iniFile);



#endif /* __INIREADER_H__ */
