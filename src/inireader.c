#include <string.h>
#include <stdlib.h>
#include "inireader.h"
#include "utils.h"

#define MAX_LINE_LEN 256
#define MAX_KEYS 20

int IsBlankline(char * sLine, int len)
{
    int i = 0;
    while ((i < len) && ((sLine[i] == ' ') || (sLine[i] == '\t') || (sLine[i] == '\n')))
    {
        i = i + 1;
    }

    if ((i == len) || (sLine[i] == '\0'))
        return -1;
    else
        return i;
}

char * SkipBlankline(FILE *fh)
{
    char * sLine;
    int i;
    sLine = malloc(sizeof(char) * MAX_LINE_LEN);
    //fgets(sLine, MAX_LINE_LEN, fh);

    do
    {
        if (fgets(sLine, MAX_LINE_LEN, fh) == NULL)
            return NULL;
        i = IsBlankline(sLine, MAX_LINE_LEN);
    }while (i == -1);

    return sLine + i;
}


char * RemoveLeadingWhiteSpace(char * str)
{
    while ((*str != '\0') && ((*str == ' ') || (*str == '\t')))
    {
        str++;
    }

    return str;
}

int GetNumSection(FILE *fh)
{
    int currentPos;
    char sLine[MAX_LINE_LEN];
    char * sLineNoSpace;
    size_t len = 0;
    int iTotalSections = 0;

    currentPos = ftell(fh);
    rewind(fh);

    while (fgets(sLine, MAX_LINE_LEN, fh) != NULL)
    {
        sLineNoSpace = RemoveLeadingWhiteSpace(sLine);
        if (*sLineNoSpace == '[')
        {
            iTotalSections = iTotalSections + 1;
        }
    }

    fseek(fh, currentPos, SEEK_SET);

    return iTotalSections;
}

struct IniFile * ReadInitFile(FILE *fh)
{
    struct IniFile * iniFile;
    int currentPos;
    int i;

    iniFile = malloc(sizeof(struct IniFile));
    iniFile->iNumSections = GetNumSection(fh);
    iniFile->iniSections = malloc(iniFile->iNumSections * sizeof(struct IniSection));

   rewind(fh);

    for (i = 0; i < iniFile->iNumSections; ++i)
    {
        iniFile->iniSections[i] = ReadInitSection(fh);
    }

   return iniFile;
}

struct IniSection ReadInitSection(FILE *fh)
{
    char * sLine;
    struct IniSection iniSec;
    int i;
    int pos;
    int len;

    iniSec.iNumberofKey = 0;
    iniSec.sName = malloc(sizeof(char) * MAX_LINE_LEN);
    // malloc a Block MAX_KEYS*MAX_LINE_LEN for Keys and Values
    iniSec.asKeys = malloc(sizeof(char *) * MAX_KEYS);
    iniSec.asValues = malloc(sizeof(char *) * MAX_KEYS);
    // First Element point to the start of block
    iniSec.asKeys[0] = malloc(sizeof(char) * MAX_KEYS * MAX_LINE_LEN);
    iniSec.asValues[0] = malloc(sizeof(char) * MAX_KEYS * MAX_LINE_LEN);
    // Assign Address to each Element, these are have MAX_LINE_LEN * i * sizeof(char) bytes offset to the base Address
    for (i = 1; i < MAX_KEYS; ++i)
    {
        iniSec.asKeys[i] = iniSec.asKeys[0] + MAX_LINE_LEN * i * sizeof(char);
        iniSec.asValues[i] = iniSec.asValues[0] + MAX_LINE_LEN * i * sizeof(char);
    }

    do
    {
        sLine = SkipBlankline(fh);

        if (sLine == NULL)
            return iniSec;

    } while (sLine[0] != '[');

    i = 0;
    sLine = sLine + 1;
    while (sLine[i] != ']')
        i = i +1;
    sLine[i] = '\0';

    strcpy(iniSec.sName, sLine);

    do
    {
        pos = ftell(fh);
        sLine = SkipBlankline(fh);
        //        printf("%s\n",sLine);
        if (sLine == NULL)
        {
            break;
        }

        i = 0;
        while (sLine[i] != '=')
            i++;
        sLine[i] = '\0';

        if (sLine[0] == '[')
        {
            free(sLine);
            break;
        }

        iniSec.iNumberofKey = iniSec.iNumberofKey + 1;
        strcpy(iniSec.asKeys[iniSec.iNumberofKey-1], sLine);
        len = strlen(sLine + i + 1);
        if (sLine[i+len] == '\n')
            sLine[i+len] = '\0';
        strcpy(iniSec.asValues[iniSec.iNumberofKey-1], sLine + i + 1);

        //free(sLine);
        RemoveWhiteSpaces(iniSec.asKeys[iniSec.iNumberofKey-1]);
        RemoveWhiteSpaces(iniSec.asValues[iniSec.iNumberofKey - 1]);

        free(sLine);

    } while (1);

    fseek(fh, pos, SEEK_SET);

    return iniSec;
}

void PrintIniSection(struct IniSection iniSec)
{
    int i;
    printf("[%s] (keys = %d)\n",iniSec.sName, iniSec.iNumberofKey);

    for (i = 0; i < iniSec.iNumberofKey; ++i)
    {
        printf("%s = %s\n", iniSec.asKeys[i], iniSec.asValues[i]);
    }

}

void PrintIniFile(struct IniFile * iniFile)
{
    int i;

    printf("Total Section = %d\n",iniFile->iNumSections);

    for (i = 0; i < iniFile->iNumSections; ++i)
    {
        PrintIniSection(iniFile->iniSections[i]);
    }

}
