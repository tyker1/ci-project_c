#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "utils.h"
#include "filterbanken.h"
#include "synthese.h"


void Mapping(double * dAcustic, int len, double * dElektrode)
{
    int i;
    for (i = 0; i < len; ++i)
    {
        dElektrode[i] = dAcustic[i] * 1.0;
    }

}

double * MaximumSelection(double * dEnvelope, int len, int iNumSelected)
{
    double * dResult;
    double * temp;
    int * idx;
    int i;

    idx = malloc(len * sizeof(double));
    dResult = calloc(len, sizeof(double));
    temp = malloc(len * sizeof(double));

    for (i = 0; i < len; ++i)
    {
        temp[i] = dEnvelope[i];
    }

    for (i = 0; i < len; ++i)
    {
        idx[i] = i;
    }

    QuickSort(temp, len, idx);


    for (i = 0; i < iNumSelected; ++i)
    {
        dResult[idx[i]] = temp[i];
    }

    free(temp);
    free(idx);
    return dResult;
}

double * PyschoacusticalSelectiion(double * dEnvelope, int len, int NumSelected, double av, double sl, double sr, double alpha, double * Labs)
{
    double * dResult;
    double dAdb[len];
    double dPowerMask[len];
    double dMax, dMask, Lt, Li;
    int i, j ,idx;

    dResult = calloc(len, sizeof(double));

    for (i = 0; i < len; ++i)
    {
        dPowerMask[i] = 0.0;
    }

    for (i = 0; i < len; ++i)
    {
        dAdb[i] = MAG2DB(dEnvelope[i]);
        //printf("%.5e\n", dAdb[i]);
    }
    //printf("======================================\n");
    for (i = 0; i < NumSelected; ++i)
    {
        dMax = -1e+20;
        idx = 0;

        // Selection
        for (j = 0; j < len; ++j)
        {
            if (dPowerMask[j] == 0.0)
            {
                dMask = dAdb[j];
            }else{
                dMask = (dAdb[j] - 10 * log10(pow(dPowerMask[j], 1.0/alpha)));
            }
            // printf("Mask = %.3e ", pow(dPowerMask[j], 1.0/alpha));
            if (dMask > dMax)
            {
                idx = j;
                dMax = dMask;
            }
        }
        dResult[idx] = dEnvelope[idx];
        // printf("\n\n");
        for (j = 0; j < len; ++j)
        {
            //spread
            if (idx > j)
            {
                Li = dMax - av - sl * (idx - j);
            } else {
                Li = dMax - av - sr * (j - idx);
            }

            if (Li < 0)
            {
                Li = 0;
            }

            // Powerlaw masking
            Lt = dPowerMask[j];
            if (Lt == 0.0)
            {
                Lt = pow(pow(10, Labs[j] / 10), alpha);
            }

            Lt = Lt + pow(pow(10, Li / 10), alpha);

            // Record
            dPowerMask[j] = Lt;
        }

    }

    return dResult;
}

double * VirtualChannelSelection(double * dEnvelope, int len, int iNumElectrode, int iNumVirtualChannel)
{
    double * dResult;
    int i;
    int j;
    int idx;
    double dMax;

    if (len != iNumElectrode * iNumVirtualChannel)
    {
        return NULL;
    }

    dResult = calloc(len, sizeof(double));

    for (i = 0; i < iNumElectrode; ++i)
    {
        dMax = -1e+20;
        idx = 0;
        for (j = 0; j < iNumVirtualChannel; ++j)
        {
            if (dEnvelope[i*iNumVirtualChannel+j] > dMax)
            {
                dMax = dEnvelope[i*iNumVirtualChannel + j];
                idx = i * iNumVirtualChannel + j;
            }
        }
        dResult[idx] = dMax;

    }

    return dResult;

}

double * ACEStrategie(struct AlgorithmusConfig config, double * dFrame, double * dSynthesed, int iReset)
{

    double * dElectrodeActivitats;
    double * dEnvelope;
    int i;
    static int iFlag = 1;
    static fftw_plan fftPlan;
    static fftw_complex * dFFTOutput;

    if (iFlag == 1)
    {
        dFFTOutput = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * config.iWindowSize);
        fftPlan = fft_InitFilter(config.iWindowSize, dFrame, dFFTOutput);
        iFlag = 0;
    } else if (iReset == 1)
    {
        fftw_destroy_plan(fftPlan);
        fftw_free(dFFTOutput);
        iFlag = 1;
        return NULL;
    }

    dEnvelope = fft_FilterBank(config, dFrame, dFFTOutput, fftPlan);

    dElectrodeActivitats = MaximumSelection(dEnvelope, config.iNumFilters, config.iNumSelected);

    Mapping(dElectrodeActivitats, config.iNumFilters, dElectrodeActivitats);

    //SinusSynthese(config, dElectrodeActivitats, dSynthesed);
    AudioSynthese(config, dElectrodeActivitats, dSynthesed);

    free(dEnvelope);

    return dElectrodeActivitats;

}

double * PACEStrategie(struct AlgorithmusConfig config, double * dFrame, double * dSynthesed, int iReset)
{
    double * dElectrodeActivitats;
    double * dEnvelope;
    double dTemp, dMin;
    int i;
    static int iFlag = 1;
    static fftw_plan fftPlan;
    static fftw_complex * dFFTOutput;
    static double * Labs;

    if (iFlag == 1)
    {
        dFFTOutput = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * config.iWindowSize);
        fftPlan = fft_InitFilter(config.iWindowSize, dFrame, dFFTOutput);

        Labs = malloc(config.iNumFilters * sizeof(double));
        dMin = 1e+10;
        for (i = 0; i < config.iNumFilters; ++i)
        {
            dTemp = config.dMidFreq[i] / 1000;
            Labs[i] = 3.64 * pow(dTemp, -0.8) - 6.5 * exp(-0.6 * SQR(dTemp-3.3)) + 1e-3*pow(dTemp, 4);
            if (Labs[i] < dMin)
            {
                dMin = Labs[i];
            }
        }

        for (i = 0; i < config.iNumFilters; ++i)
        {
            Labs[i] = Labs[i] - dMin;
        }


        iFlag = 0;
    } else if (iReset == 1)
    {
        fftw_destroy_plan(fftPlan);
        fftw_free(dFFTOutput);
        free(Labs);
        iFlag = 1;
        return NULL;
    }

    dEnvelope = fft_FilterBank(config, dFrame, dFFTOutput, fftPlan);

    dElectrodeActivitats = PyschoacusticalSelectiion(dEnvelope, config.iNumFilters, config.iNumSelected, config.dAttenuation, config.dLeftSlope, config.dRightSlope, config.dAlpha, Labs);

    Mapping(dElectrodeActivitats, config.iNumFilters, dElectrodeActivitats);

    ///SinusSynthese(config, dElectrodeActivitats, dSynthesed);
    AudioSynthese(config, dElectrodeActivitats, dSynthesed);

    free(dEnvelope);

    return dElectrodeActivitats;
}

double * APACEvStrategie(struct AlgorithmusConfig config, double * dFrame, double * dSynthesed, int iReset)
{
    double * dElectrodeActivitats;
    double * dEnvelope;
    double * dEletrodeEnvelope;
    double dTemp, dMin;
    int i;
    static int iFlag = 1;
    static fftw_plan fftPlan;
    static fftw_complex * dFFTOutput;
    static double * Labs;

    if (iFlag == 1)
    {
        dFFTOutput = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * config.iWindowSize);
        fftPlan = fft_InitFilter(config.iWindowSize, dFrame, dFFTOutput);

        Labs = malloc(config.iNumFilters * sizeof(double));
        dMin = 1e+10;
        for (i = 0; i < config.iNumFilters; ++i)
        {
            dTemp = config.dMidFreq[i] / 1000;
            Labs[i] = 3.64 * pow(dTemp, -0.8) - 6.5 * exp(-0.6 * SQR(dTemp-3.3)) + 1e-3*pow(dTemp, 4);
            if (Labs[i] < dMin)
            {
                dMin = Labs[i];
            }
        }

        for (i = 0; i < config.iNumFilters; ++i)
        {
            Labs[i] = Labs[i] - dMin;
        }


        iFlag = 0;
    } else if (iReset == 1)
    {
        fftw_destroy_plan(fftPlan);
        fftw_free(dFFTOutput);
        free(Labs);
        iFlag = 1;
        return NULL;
    }

    dEnvelope = fft_FilterBank(config, dFrame, dFFTOutput, fftPlan);

    dEletrodeEnvelope = VirtualChannelSelection(dEnvelope, config.iNumFilters, config.iNumChannel, config.iNumVirtChannel);

    dElectrodeActivitats = PyschoacusticalSelectiion(dEletrodeEnvelope, config.iNumFilters, config.iNumSelected, config.dAttenuation, config.dLeftSlope, config.dRightSlope, config.dAlpha, Labs);

    Mapping(dElectrodeActivitats, config.iNumFilters, dElectrodeActivitats);

    //SinusSynthese(config, dElectrodeActivitats, dSynthesed);
    AudioSynthese(config, dElectrodeActivitats, dSynthesed);

    free(dEnvelope);
    free(dEletrodeEnvelope);

    return dElectrodeActivitats;
}
