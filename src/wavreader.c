#include "utils.h"
#include "wavreader.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define WAV_PCM 1
#define WAV_FMTLEN 16

// #define DEBUG


int CheckWavEnd(struct WavHeader * header, int reset)
{
    static int64_t iRestData = 0;
    static int bFirstTime = 1;


    if ((reset == 1) || (bFirstTime == 1))
    {
        iRestData = header->iDataSize;
        bFirstTime = 0;
        return iRestData;
    }

    return iRestData--;
}

struct WavHeader * readWaveHeader(FILE *fh)
{
    struct WavHeader * header;
    header = malloc(sizeof(struct WavHeader));
    fread(header, sizeof(struct WavHeader), 1, fh);
    return header;
}

struct WavHeader * createWaveHeader (uint16_t channels, uint32_t samplerate, uint16_t bits)
{
    struct WavHeader * header;
    header = malloc(sizeof(struct WavHeader));
    header->iChannels = channels;
    header->iSampleRate = samplerate;
    header->iBitsPerSample = bits;
    strncpy(header->cData, "data", 4);
    strncpy(header->cRiff, "RIFF", 4);
    strncpy(header->cFmtMarker, "fmt ", 4);
    strncpy(header->cWaveMark, "WAVE", 4);
    header->uiFileSize = 36;
    header->iFmtLen = WAV_FMTLEN;
    header->iFmtType = WAV_PCM;
    header->iDataSize = 0;
    header->iByteRate = samplerate * (bits/8) * channels;
    header->iAlignment = (bits/8) * channels;
    return header;
}



void printWaveHeader (struct WavHeader * header)
{
    char tempStr[20];
    strncpy(tempStr, header->cRiff, sizeof(header->cRiff));
    printf("RIFF = %s\n", tempStr);
    printf("Size of File = %d Bytes\n", header->uiFileSize);
    strncpy(tempStr, header->cWaveMark, sizeof(header->cWaveMark));
    printf("WAVE = %s\n", tempStr);
    strncpy(tempStr, header->cFmtMarker, sizeof(header->cFmtMarker));
    printf("FormatMark = %s\n", tempStr);
    printf("Rest Data in Format = %d Bytes\n", header->iFmtLen );
    printf("Format Type = %d \n", header->iFmtType);
    printf("Number of Channels = %d \n", header->iChannels);
    printf("Samplerate = %d Hz \n", header->iSampleRate);
    printf("Byterate = %d Bps \n", header->iByteRate);
    printf("Framesize = %d Bytes pro Frame \n", header->iAlignment);
    printf("Bits per Sample = %d bits \n", header->iBitsPerSample);
    strncpy(tempStr, header->cData, sizeof(header->cData));
    printf("DATA = %s \n", tempStr);
    printf("Data Segment Size = %d Bytes \n", header->iDataSize);
}

void _int8ToDouble(unsigned char * buffer, int buflen, double *output)
{
    int i;
    for (i = 0; i < buflen; ++i)
    {
        output[i] = 1.0 * buffer[i] / ((1 << 7) - 1); // convert to double, int8 means 2^-7 ~ (2^7-1)
    }

}

void _int16ToDouble(unsigned char * buffer, int buflen, double *output)
{
    int i;
    int16_t temp;
    int16_t test;
    int16_t iMax;
    static int position = 0x2c;
    iMax = (1 << (16-1)) - 1; // maximun for int16

    for (i = 0; i < buflen/2; ++i) // because buflen is byte number buflen/2 = length in int16
    {
        //printf("Position:%X\t RawData=%02X %02X\t", position, buffer[i*2] & 0xff, buffer[i*2+1] & 0xff);
        temp = (buffer[i*2+1] << 8) | (buffer[i*2]); // its little endian
        //test = (buffer[i*2+1] << 8) + (buffer[i*2] & 0xff);
        //printf("high=%04X\t", test & 0x0ffff);
        // printf("converted=%d(%04X)\n",temp,temp & 0x0ffff);
        position= position + 2;
        output[i] = 1.0 * temp / iMax;
    }

}

void _int32ToDouble(unsigned char *buffer, int buflen, double *output)
{
    int i;
    int32_t temp;
    int32_t iMax;

    iMax = (1 << (32 - 1)) - 1; // Max Int32

    for (i = 0; i < buflen/4; ++i) //buflen in int32 = buflen in byte / 4
    {
        temp = (buffer[i*4+3] << 24) | (buffer[i*4+2] << 16)
               | (buffer[i*4+1] << 8) | (buffer[i*4]); // little endian
        output[i] = 1.0 * temp / iMax;
    }

}

int WaveReadFrame(FILE *fh, struct WavHeader * header,int iFrameSize, double *output)
{
    unsigned char buffer[iFrameSize * header->iChannels * (header->iBitsPerSample / 8)]; // buffer of raw data
    int i,j;
    int iBytesRead;
    int temp;
    int byteLen = header->iBitsPerSample / 8;

    iBytesRead = 0;
    if (!feof(fh))
    {
        //iBytesRead = fread(buffer, sizeof(buffer[0]), sizeof(buffer), fh);
        for (i = 0; i < iFrameSize*byteLen*header->iChannels; ++i)
        {
            if (CheckWavEnd(header, 0) > 0)
            {
                temp = fread(buffer+i, sizeof(buffer[0]),1,fh);
                if (temp > 0)
                {
                    iBytesRead = iBytesRead + temp;
                }
                else
                {
                    //printf("Error while read Bytes\n");
                    break;
                }
            }
        }

        switch (byteLen)
        {
            case 1:
                {
                    _int8ToDouble(buffer, iBytesRead, output);
                    break;
                }
            case 2:
                {
                    _int16ToDouble(buffer, iBytesRead, output);
                    break;
                }
            case 4:
                {
                    _int32ToDouble(buffer, iBytesRead, output);
                    break;
                }
        }
        return iBytesRead;
    }
    else
    {
        return -1;
    }
}

int _WriteLittleEndian(int32_t data, int byteLen, FILE * fh)
{
    int8_t temp;
    int i,total;
    total = 0;

    // printf("Data = %d\t byteLen = %d\t FilePos = %d\n",data,byteLen,ftell(fh));

    for (i = 0; i < byteLen; ++i)
    {

        temp = data & 0xff;
        data = data >> 8;
        total = total + fwrite(&temp, sizeof(temp), 1, fh);
    }

    return total;
}

int _WaveWriteFrame8b(FILE *fh, struct WavHeader * header, int iFrameSize, double * frame)
{
    int i, iTotalLen;
    int8_t *iFrame;
    int8_t iMax;

    iMax = (1 << (sizeof(int8_t)*8-1)) - 1;
    iTotalLen = iFrameSize * header->iChannels;
    iFrame = malloc(sizeof(int8_t) * iFrameSize * header->iChannels);

    for (i = 0; i < iTotalLen; ++i)
    {
        iFrame[i] = (int8_t)trunc(frame[i] * iMax);
    }

    return fwrite(iFrame, sizeof(iFrame[0]), iTotalLen, fh);

}

int _WaveWriteFrame16b(FILE *fh, struct WavHeader * header, int iFrameSize, double * frame)
{
    int i, iTotalLen;
    int16_t *iFrame;
    uint16_t iMax;
    int iTotalWrite = 0;

    iMax = (1 << (sizeof(int16_t)*8-1)) - 1;
    iTotalLen = iFrameSize * header->iChannels;
    iFrame = malloc(sizeof(int16_t) * iFrameSize * header->iChannels);
    //    printf("Here: iMax = %d, iTotalLen = %d, first Sample = %f\n",iMax,iTotalLen,frame[0]);
    for (i = 0; i < iTotalLen; ++i)
    {
        iFrame[i] = round(frame[i] * iMax);
        //  printf("%0.4f\t%d\n",frame[i],iFrame[i]);
        iTotalWrite = iTotalWrite + _WriteLittleEndian(iFrame[i], 2, fh);
    }
    //   printf("iFrame[0] Size = %d\n", iMax);
    //return fwrite(iFrame, sizeof(iFrame[0]), iTotalLen, fh);
    return iTotalWrite;

}

int _WaveWriteFrame32b(FILE *fh, struct WavHeader * header, int iFrameSize, double * frame)
{
    int i, iTotalLen;
    int32_t *iFrame;
    uint32_t iMax;
    int iTotalWrite = 0;

    iMax = (1 << (sizeof(int32_t)*8-1)) - 1;
    iTotalLen = iFrameSize * header->iChannels;
    iFrame = malloc(sizeof(int32_t) * iFrameSize * header->iChannels);

    for (i = 0; i < iTotalLen; ++i)
    {
        iFrame[i] = (int32_t)trunc(frame[i] * iMax);
        iTotalWrite = iTotalWrite + _WriteLittleEndian(iFrame[i], 4, fh);
    }

    // return fwrite(iFrame, sizeof(iFrame[0]), iTotalLen, fh);
    return iTotalWrite;

}

int WaveWriteFrame(FILE *fh, struct WavHeader * header, int iFrameSize, double *frame)
{
    int iBytesWritten;
    int iByteLen = header->iBitsPerSample / 8;
    switch (iByteLen)
    {
        case 1:
            {
                iBytesWritten = _WaveWriteFrame8b(fh, header, iFrameSize, frame);
                break;
            }
        case 2:
            {
                iBytesWritten = _WaveWriteFrame16b(fh, header, iFrameSize, frame);
                break;
            }
        case 4:
            {
                iBytesWritten = _WaveWriteFrame32b(fh, header, iFrameSize, frame);
                break;
            }
        default:
            return -1;
    }

    header->iDataSize = header->iDataSize + iBytesWritten;
    header->uiFileSize = header->uiFileSize + iBytesWritten;

    return iBytesWritten;

}

void WaveWriteHeader(FILE *fh, struct WavHeader * header)
{
    int currentPos;

    currentPos = ftell(fh);
    rewind(fh);

    fwrite(header, sizeof(struct WavHeader), 1, fh);
    if (currentPos != 0)
        fseek(fh, currentPos, SEEK_SET);
}
