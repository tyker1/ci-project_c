#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "synthese.h"


void AudioSynthese(struct AlgorithmusConfig config, double * dEnvelope, double * dSynthesed)
{
    switch (config.iSyntheseModel)
    {
        case SINUS:
            {
                SinusSynthese(config, dEnvelope, dSynthesed);
                break;
            }
        default:
            break;
    }
}

void SinusSynthese(struct AlgorithmusConfig config, double * dEnvelope, double * dSynthesed)
{
    static double lastAmp[MAX_CHANNEL] = {0};
    static double lastPhi[MAX_CHANNEL] = {0};
    int i,j;
    double * dAmpInt;
    double dLast, dCurrent, dDelta;
    double dTimeStep, dTimeStart;

    for (i = 0; i < config.iFrameSize; ++i)
    {
        dSynthesed[i] = 0;
    }

    //dAmpInt = malloc(sizeof(double) * config.iFrameSize);

    dTimeStep = 1.0 / config.iSamplerate;

    if (config.iNumFilters > MAX_CHANNEL)
    {
        dSynthesed[0] = -20;
        return;
    }

    for (i = 0; i < config.iNumFilters; ++i)
    {
        dLast = lastAmp[i];
        dCurrent = dEnvelope[i];
        dDelta = (dCurrent - dLast) / config.iFrameSize;

        if ((dLast == 0) && (dCurrent == 0))
        {
            // printf("i = %d continue!\n",i);
            continue;
        }
        /* printf("==========================================\n");*/
        dTimeStart = lastPhi[i];
        /*
          printf("dLast = %.5e\tdCurrent = %.5e\tdDelta = %.5e\tdTimeStart = %.5e\n",dLast, dCurrent, dDelta, dTimeStart);*/

        for (j = 0; j < config.iFrameSize; ++j)
        {
            dSynthesed[j] = dSynthesed[j] + (dLast + dDelta * j) * sin(2 * M_PI * (dTimeStart + dTimeStep * j) * config.dMidFreq[i]);

        }

        lastAmp[i] = dCurrent;
        lastPhi[i] = dTimeStart + dTimeStep * config.iFrameSize;

    }
}

void LinearInterpolation(double * dInt, double dStart, double dEnd, int iSteps)
{
    double dDelta = (dEnd - dStart) / iSteps;
    int i;

    for (i = 0; i < iSteps; ++i)
    {
        dInt[i] = dStart + dDelta * i;
    }

}
