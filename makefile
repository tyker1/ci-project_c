CC=gcc
CFLAGS=-I../lib/fftw-3.3.5-dll32 -I./include
LDFLAGS=-L../lib/fftw-3.3.5-dll32
LIBS=-lfftw3-3 -lm

SRCDIR = ./src/
HDIR = ./include/

ifeq ($(OS), Windows_NT)
	CLEAN = del
else
	CLEAN = rm -f
endif

SRCS = main.c wavreader.c utils.c inireader.c strategien.c synthese.c filterbanken.c

OBJS = main.o wavreader.o utils.o inireader.o strategien.o synthese.o filterbanken.o
OBJECT = $(patsubst %.o,$(SRCDIR)%.o,$(OBJS))
SOURCE = $(patsubst %.c,$(SRCDIR)%.c,$(SRCS))

all: advancedPACEv

debug: CFLAGS += -g
debug: advancedPACEv

advancedPACEv : $(OBJECT)
	$(CC) -o advancedPACEv.exe $(OBJECT) $(LDFLAGS) $(LIBS)

$(OBJECT) : $(HDIR)utils.h

$(SRCDIR)wavreader.o: $(HDIR)wavreader.h

$(SRCDIR)inireader.o: $(HDIR)inireader.h

$(SRCDIR)strategien.o: $(HDIR)strategien.h $(SRCDIR)synthese.o $(SRCDIR)filterbanken.o

$(SRCDIR)synthese.o: $(HDIR)synthese.h

$(SRCDIR)filterbanken.o: $(HDIR)filterbanken.h

.PHONY: clean

clean:
	$(RM) $(OBJECT)
	$(RM) advancedPACEv.exe
